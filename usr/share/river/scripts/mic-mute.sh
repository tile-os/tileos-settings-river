#!/bin/bash

set -e

MIC="${1:-'Capture'}"

amixer sset "$MIC" toggle

STATE="$(amixer sget "$MIC",0 | sed -E -e '0,/^.*\[(on|off)\].*$/d' -e 's/^.*\[(\w+)\].*$/\1/')"

test -n "$STATE" && notify-send \
    --app-name river \
    --expire-time 800 \
    "Microphone is $STATE"
