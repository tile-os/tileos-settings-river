#!/bin/sh
/usr/share/river/scripts/sbdp.py $HOME/.config/river/keybindings.sh | jq --raw-output 'sort_by(.category) | .[] | .action + ": <b>" + .keybinding + "</b>"'
