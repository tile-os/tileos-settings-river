#!/usr/bin/env bash

# Use the "logo" key as the primary modifier
mod="Mod4"

# Default terminal emulator
term="alacritty"

# Application launcher
launcher="eval rofi -show combi -combi-modi 'drun,run' -terminal $term -ssh-command '{terminal} {ssh-client} {host} [-p {port}]' -run-shell-command '{terminal} {cmd}'"

# Volume changing notify
volume_bar="/usr/share/river/scripts/volume-notify.sh"

# Mic volume mute
mic_mute="/usr/share/river/scripts/mic-mute.sh"

# Brightness changing notify
brightness_bar="/usr/share/river/scripts/brightness-notify.sh"

# Screenshot notify
screenshot_notify="eval [[ $(wl-paste -l) == "image/png" ]] && notify-send 'Screenshot copied to clipboard'"

# Screenshot scripts
riverctl map normal "None" Print spawn "/usr/bin/river-grimshot save screen - | swappy -f - && bash -c $screenshot_notify"
riverctl map normal "$mod" Print spawn "/usr/bin/river-grimshot save area - | swappy -f - && bash -c $screenshot_notify"

# Set keyboard layout. See man riverctl
# riverctl keyboard-layout -options "grp:caps_toggle" "us,ru"

## Action // Reload River Configuration // $mod+R ##
riverctl map normal $mod R spawn $HOME/.config/river/init

## Launch // Terminal // $mod+Return ##
riverctl map normal $mod Return spawn $term

## Launch // Launcher // $mod+D ##
riverctl map normal $mod D spawn "$launcher"

## Action // Exit River // $mod+Shift+E ##
riverctl map normal $mod+Shift E spawn nwg-bar

## Action // Kill focused window // $mod+Q ##
riverctl map normal $mod+Shift Q close

## Navigation // Focus the next/previous view in the layout stack // $mod+{J,K} ##
riverctl map normal $mod J focus-view next
riverctl map normal $mod K focus-view previous

## Navigation // Swap the focused view in the layout stack // $mod+Shift+{J,K} ##
riverctl map normal $mod+Shift J swap next
riverctl map normal $mod+Shift K swap previous

## Navigation // Focus the next/previous output // $mod+{, .} ##
riverctl map normal $mod Period focus-output next
riverctl map normal $mod Comma focus-output previous

## Navigation // Send the focused view to the next/previous output // $mod+Shift+{, .} ##
riverctl map normal $mod+Shift Period send-to-output next
riverctl map normal $mod+Shift Comma send-to-output previous

## Navigation // Bump the focused view to the top of the layout stack // $mod+Shift+Return ##
riverctl map normal $mod+Shift Return zoom

## Navigation // Decrease/increase the main ratio of rivertile // $mod+{H,L} ##
riverctl map normal $mod H send-layout-cmd rivertile "main-ratio -0.05"
riverctl map normal $mod L send-layout-cmd rivertile "main-ratio +0.05"

## Navigation // Increment/decrement the main count of rivertile // $mod+Shift+{H,L} ##
riverctl map normal $mod+Shift H send-layout-cmd rivertile "main-count +1"
riverctl map normal $mod+Shift L send-layout-cmd rivertile "main-count -1"

## Navigation // Move views // $mod+Alt+{H,J,K,L} ##
riverctl map normal $mod+Alt H move left 100
riverctl map normal $mod+Alt J move down 100
riverctl map normal $mod+Alt K move up 100
riverctl map normal $mod+Alt L move right 100

## Navigation // Snap views to screen edges // $mod+Alt+Control+{H,J,K,L}##
riverctl map normal $mod+Alt+Control H snap left
riverctl map normal $mod+Alt+Control J snap down
riverctl map normal $mod+Alt+Control K snap up
riverctl map normal $mod+Alt+Control L snap right

## Navigation // Resize views // $mod+Alt+Shift+{H,J,K,L} ##
riverctl map normal $mod+Alt+Shift H resize horizontal -100
riverctl map normal $mod+Alt+Shift J resize vertical 100
riverctl map normal $mod+Alt+Shift K resize vertical -100
riverctl map normal $mod+Alt+Shift L resize horizontal 100

## Navigation // Move views with mouse // $mod + Left Mouse Button ##
riverctl map-pointer normal $mod BTN_LEFT move-view

## Navigation // Resize views with mouse // $mod + Right Mouse Button ##
riverctl map-pointer normal $mod BTN_RIGHT resize-view

## Navigation // Toggle float // $mod + Middle Mouse Button ##
riverctl map-pointer normal $mod BTN_MIDDLE toggle-float

# Tags navigation

## Navigation // Focus tag [0-8] // $mod+[1-9] ##
## Navigation // Tag focused view with tag [0-8] // $mod+Shift+[1-9] ##
## Navigation // Toggle focus of tag [0-8] // mod+Control+[1-9] ##
## Navigation // Toggle tag [0-8] of focused view // $mod+Shift+Control+[1-9] ##

for i in $(seq 1 9)
do
    tags=$((1 << ($i - 1)))

    riverctl map normal $mod $i set-focused-tags $tags

    riverctl map normal $mod+Shift $i set-view-tags $tags

    riverctl map normal $mod+Control $i toggle-focused-tags $tags

    riverctl map normal $mod+Shift+Control $i toggle-view-tags $tags
done

## Navigation // Focus all tags // $mod+0 ##
## Navigation // Tag focused view with all tags // $mod+Shift+0 ##
all_tags=$(((1 << 32) - 1))
riverctl map normal $mod 0 set-focused-tags $all_tags
riverctl map normal $mod+Shift 0 set-view-tags $all_tags

## Action // Toggle floating // $mod+Space ##
riverctl map normal $mod Space toggle-float

## Action // Toggle fullscreen // $mod+F ##
riverctl map normal $mod F toggle-fullscreen

## Navigation // Change layout orientation // $mod+{↑ ↓ ← →} ##
riverctl map normal $mod Up    send-layout-cmd rivertile "main-location top"
riverctl map normal $mod Right send-layout-cmd rivertile "main-location right"
riverctl map normal $mod Down  send-layout-cmd rivertile "main-location bottom"
riverctl map normal $mod Left  send-layout-cmd rivertile "main-location left"

# Declare a passthrough mode. This mode has only a single mapping to return to
# normal mode. This makes it useful for testing a nested wayland compositor
riverctl declare-mode passthrough

## Action // Enter passthrough mode // $mod+F11 ##
riverctl map normal $mod F11 enter-mode passthrough

## Action // Return to normal mode // $mod+F11 ##
riverctl map passthrough $mod F11 enter-mode normal

# Various media key mapping examples for both normal and locked mode which do
# not have a modifier
for mode in normal locked
do
    # Eject the optical drive (well if you still have one that is)
    riverctl map $mode None XF86Eject spawn 'eject -T'

    # Control pulse audio volume with pamixer (https://github.com/cdemoulins/pamixer)
    riverctl map $mode None XF86AudioRaiseVolume  spawn "pulsemixer --change-volume +5 && $volume_bar"
    riverctl map $mode None XF86AudioLowerVolume  spawn "pulsemixer --change-volume -5 && $volume_bar"
    riverctl map $mode None XF86AudioMute         spawn "pulsemixer --toggle-mute && $volume_bar"
    riverctl map $mode None XF86AudioMicMute      spawn "$mic_mute"

    # Control MPRIS aware media players with playerctl (https://github.com/altdesktop/playerctl)
    riverctl map $mode None XF86AudioMedia spawn 'playerctl play-pause'
    riverctl map $mode None XF86AudioPlay  spawn 'playerctl play-pause'
    riverctl map $mode None XF86AudioPrev  spawn 'playerctl previous'
    riverctl map $mode None XF86AudioNext  spawn 'playerctl next'

    # Control screen backlight brightness with light (https://github.com/haikarainen/light)
    riverctl map $mode None XF86MonBrightnessUp   spawn "light -A 5 && $brightness_bar"
    riverctl map $mode None XF86MonBrightnessDown spawn "light -U 5 && $brightness_bar"
done

# Set keyboard repeat rate
riverctl set-repeat 50 300
